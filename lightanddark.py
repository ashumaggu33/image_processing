import cv2
import numpy as np
from PIL import Image
from flask import Flask,request
import os
import base64
import json
from PIL import Image,ImageEnhance
from pprint import pprint


#, methods=['POST', 'GET'])
app = Flask(__name__)
@app.route('/number-detect', methods=['POST'])
def decrypt():

    #To request data from  json
    try:
        # python3
        req_data = request.json
    except:
        # python2
        req_data = request.get_json()
    image1 = req_data['ByteData']


    data = image1  # string
    data = image1.encode()  # bytes
    data = b"(image1)"  # bytes

    bytes1 = bytes(image1, 'utf-8')

    with open("base64.png", "wb") as fh:
        fh.write(base64.decodebytes(bytes1))

    #i = input("Enter image name with specified format :-")
    image = Image.open('base64.png')
    img_gray = cv2.imread('base64.png',0)
    template = cv2.imread('test.png',0)

    contrast = ImageEnhance.Contrast(image)
    enhanced_img=contrast.enhance(40)
    enhanced_img.save("enhanced_shade1.png")
    enhanced_img2=cv2.imread("enhanced_shade1.png")

    #template matching and finding its coordinate
    res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = list(max_loc)
    print(top_left)
    check_0=[top_left[0]+7,top_left[1]-38,top_left[0]+27,top_left[1]-28]
    check_1=[top_left[0]+39,top_left[1]-35,top_left[0]+59,top_left[1]-25]
    check_2=[top_left[0]+70,top_left[1]-11,top_left[0]+90,top_left[1]-1]
    check_3=[top_left[0]+66,top_left[1]+16,top_left[0]+86,top_left[1]+26]
    check_4=[top_left[0]+58,top_left[1]+44,top_left[0]+78,top_left[1]+54]
    check_5=[top_left[0]+40,top_left[1]+62,top_left[0]+60,top_left[1]+72]
    check_6=[top_left[0]+4,top_left[1]+69,top_left[0]+24,top_left[1]+79]
    check_7=[top_left[0]-26,top_left[1]+66,top_left[0]-6,top_left[1]+76]
    check_8=[top_left[0]-50,top_left[1]+44,top_left[0]-30,top_left[1]+54]
    check_9=[top_left[0]-53,top_left[1]+17,top_left[0]-33,top_left[1]+27]
    check_A=[top_left[0]-40,top_left[1]-11,top_left[0]-20,top_left[1]-1]
    check_B=[top_left[0]-28,top_left[1]-30,top_left[0]-8,top_left[1]-20]

    all_boxes=[check_0,check_1,check_2,check_3,check_4,check_5,check_6,check_7,check_8,check_9,check_A,check_B]
    print(all_boxes)
    final_list=[]
    dark_blue=[255,0,0]
    light_blue=[255,255,0]
    black=[0,0,0]

    def checkPixel(rect,img,color):
        count_yes=0
        for x in range(rect[0],rect[2]+1):
            for y in range(rect[1],rect[3]+1):
                if(color==black):
                    if(binary[y][x]==0):
                        count_yes+=1
                else:
                    if((img[y][x][0]==color[0]) and (img[y][x][1]==color[1]) and (img[y][x][2]==color[2])):
                        count_yes+=1
        print('count_yes ',count_yes)
        if(count_yes>=30):
            return True
        else:
            return False
        
    def CreateList(all_boxes,image,color):
        for box in all_boxes:
            if(checkPixel(box,image,color)):
                print(all_boxes.index(box))
                if(all_boxes.index(box)==10):
                    final_list.append("A")
                if(all_boxes.index(box)==11):
                    final_list.append("B")
                if((all_boxes.index(box)>=0) and (all_boxes.index(box)<=9)):
                    final_list.append(str(all_boxes.index(box)))
        return final_list

    l_dark_blue = CreateList(all_boxes,enhanced_img2,dark_blue)
    print('l3'*10)
    print(l_dark_blue)
    print('l3' * 10)
    if(final_list==[]):
        print("===now checking for lightblue===")
        l_light_blue = CreateList(all_boxes,enhanced_img2,light_blue)
        print('l1'*20)
        print(l_light_blue)
        print('l1' * 20)
        print(final_list)
        if(final_list==[]):
            print("===now checking for binary===")
            ret,binary = cv2.threshold(img_gray,190,255,cv2.THRESH_BINARY)
            binary = cv2.medianBlur(binary,5)
            l_black = CreateList(all_boxes,binary,black)
            print('l2' * 20)
            print(l_black)
            print('l2' * 20)
            str1 = ','.join(final_list)
            result='{ "result" : "'+str1+'" }'
            return str(result)
        else:

            str1 = ','.join(final_list)
            result='{ "result" : "'+str1+'" }'
            return str(result)

    else:
        print("===checking for darkblue===")
        str1 = ','.join(final_list)
        result='{ "result" : "'+str1+'" }'
        return str(result)


if __name__ == '__main__':
    app.run(debug=True,port="4000")

#decrypt()
