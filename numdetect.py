import cv2
import numpy as np
from PIL import Image
from flask import Flask, request
import os
import base64
import json
from PIL import Image, ImageEnhance
from pprint import pprint
import pyzbar.pyzbar as pyzbar
# , methods=['POST', 'GET'])
app = Flask(__name__)


@app.route('/number-detect', methods=['POST', 'GET'])
def decrypt():
    # To request data from  json
    req_data = request.get_json()
    image = req_data['ByteData']

    data = image  # string
    data = image.encode()  # bytes
    data = b"(image)"  # bytes

    bytes1 = bytes(image, 'utf-8')

    with open("base64.png", "wb") as fh:
        fh.write(base64.decodebytes(bytes1))

    img_temp = cv2.imread('base64.png')
    img_temp = cv2.resize(img_temp, (355, 282), interpolation=cv2.INTER_AREA)
    cv2.imwrite('base64.png', img_temp)

    image = Image.open('base64.png')
    img_gray = cv2.imread('base64.png', 0)
    contrast = ImageEnhance.Contrast(image)
    enhanced_img = contrast.enhance(2)
    enhanced_img.save("enhanced_shade1.png")
    enhanced_img = cv2.imread("enhanced_shade1.png")


    # Decode QR Code an Get Test ID from the same
    decodedObjects = pyzbar.decode(enhanced_img)
    qr_code_pattern, test_id = '', ''
    for obj in decodedObjects:
        qr_code_pattern = obj.data
        test_id = str(qr_code_pattern, 'utf-8').split('=')[1]
    # Read Dial Pattern

    # width_height = []
    t_0 = cv2.imread('0t.png', 0)
    # w, h = t_0.shape[::-1]
    # width_height.append([w,h])
    t_1 = cv2.imread('1t.png', 0)
    # w, h = t_1.shape[::-1]
    # width_height.append([w, h])
    t_2 = cv2.imread('2t.png', 0)
    # w, h = t_2.shape[::-1]
    # width_height.append([w, h])
    t_3 = cv2.imread('3t.png', 0)
    # w, h = t_3.shape[::-1]
    # width_height.append([w, h])
    t_4 = cv2.imread('4t.png', 0)
    # w, h = t_4.shape[::-1]
    # width_height.append([w, h])
    t_5 = cv2.imread('5t.png', 0)
    # w, h = t_5.shape[::-1]
    # width_height.append([w, h])
    t_6 = cv2.imread('6t.png', 0)
    # w, h = t_6.shape[::-1]
    # width_height.append([w, h])
    t_7 = cv2.imread('7t.png', 0)
    # w, h = t_7.shape[::-1]
    # width_height.append([w, h])
    t_8 = cv2.imread('8t.png', 0)
    # w, h = t_8.shape[::-1]
    # width_height.append([w, h])
    t_9 = cv2.imread('9t.png', 0)
    # w, h = t_9.shape[::-1]
    # width_height.append([w, h])
    t_A = cv2.imread('At.png', 0)
    # w, h = t_A.shape[::-1]
    # width_height.append([w, h])
    t_B = cv2.imread('Bt.png', 0)
    # w, h = t_B.shape[::-1]
    # width_height.append([w, h])
    # print("wid",width_height)
    l_0=[]
    l_1=[]
    l_2=[]
    l_3=[]
    l_4=[]
    l_5=[]
    l_6=[]
    l_7=[]
    l_8=[]
    l_9=[]
    l_A=[]
    l_B=[]
    test_images=[t_0,t_1,t_2,t_3,t_4,t_5,t_6,t_7,t_8,t_9,t_A,t_B]
    test_images_loc=[l_0,l_1,l_2,l_3,l_4,l_5,l_6,l_7,l_8,l_9,l_A,l_B]
    ret, binary = cv2.threshold(img_gray, 190, 255, cv2.THRESH_BINARY)
    binary = cv2.medianBlur(binary, 5)
    for n in range(0,12):
        res = cv2.matchTemplate(img_gray,test_images[n], cv2.TM_CCOEFF_NORMED)
        # print(res)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        # print("min",min_loc)
        # print("max",max_loc)
        test_images_loc[n]=list(max_loc)
    print(test_images_loc)
    check_0 = [test_images_loc[0][0]-7 ,test_images_loc[0][1] + 20]
    check_1 = [test_images_loc[1][0]-19 ,test_images_loc[1][1] + 25]
    check_2 = [test_images_loc[2][0]-30 ,test_images_loc[2][1] + 14]
    check_3 = [test_images_loc[3][0]-28 ,test_images_loc[3][1] + 1]
    check_4 = [test_images_loc[4][0]-27 ,test_images_loc[4][1] - 9]
    check_5 = [test_images_loc[5][0]-24 ,test_images_loc[5][1] - 18]
    check_6 = [test_images_loc[5][0]-55 ,test_images_loc[5][1] - 15]
    check_7 = [test_images_loc[7][0]+7 ,test_images_loc[7][1] - 20]
    check_8 = [test_images_loc[8][0]+17 ,test_images_loc[8][1] - 15]
    check_9 = [test_images_loc[8][0]+7 ,test_images_loc[8][1] - 40]
    check_A = [test_images_loc[10][0]+18 ,test_images_loc[10][1] + 16]
    check_B = [test_images_loc[11][0]+ 15 ,test_images_loc[11][1] + 15]

    all_boxes = [check_0, check_1, check_2, check_3,check_4,check_5,check_6,check_7,check_8,check_9,check_A,check_B]

    final_list = []
    dark_blue = [255, 0, 0]
    light_blue = [255, 255, 0]
    black = [0, 0, 0]

    def checkPixel(rect, img, color):
        count_yes =0

        for x in range(rect[0], rect[0] + 25):
            for y in range(rect[1], rect[1]+17):
                if (color == black):
                    if (binary[y][x] == 0):
                        count_yes += 1
                else:
                    # print(img[y][x])
                    # if ((img[y][x][0] == color[0]) and (img[y][x][1] == color[1]) and (img[y][x][2] == color[2])):
                    if ((img[y][x][0] == color[0]) and (img[y][x][1] == color[1]) and (img[y][x][2] <= 254)):
                        count_yes += 1
        # print("count",count_yes)
        if (count_yes >= 20):
            return True

    def CreateList(all_boxes, image, color):
        for box in all_boxes:
            # print("BOX NO ",all_boxes.index(box))
            if box != '':
                if (checkPixel(box, image, color)):

                    # print("hi")
                    if (all_boxes.index(box) == 10):
                        final_list.append("A")
                    if (all_boxes.index(box) == 11):
                        final_list.append("B")
                    if ((all_boxes.index(box) >= 0) and (all_boxes.index(box) <= 9)):
                        final_list.append(str(all_boxes.index(box)))

    CreateList(all_boxes, enhanced_img, dark_blue)

    if (final_list == []):
        print("===now checking for lightblue===")
        CreateList(all_boxes, enhanced_img, light_blue)
        print(final_list)
        if (final_list == []):
            # print("hi")
            print("===now checking for binary===")
            ret, binary = cv2.threshold(img_gray, 190, 255, cv2.THRESH_BINARY)
            binary = cv2.medianBlur(binary, 5)
            CreateList(all_boxes, binary, black)
            str1 = ','.join(final_list)
            print(str1)
            return {'result': str1, 'QR code': str(qr_code_pattern, 'utf-8'), 'test_id':test_id[:5]+'-'+test_id[5:]}
        else:
            str1 = ','.join(final_list)
            return {'result': str1, 'QR code': str(qr_code_pattern, 'utf-8'), 'test_id':test_id[:5]+'-'+test_id[5:]}

    else:
        print("===checking for darkblue===")
        str1 = ','.join(final_list)
        return {'result': str1, 'QR code': str(qr_code_pattern, 'utf-8'), 'test_id':test_id[:5]+'-'+test_id[5:]}
#
#
if __name__ == '__main__':
    app.run(debug=True, port="5000")
